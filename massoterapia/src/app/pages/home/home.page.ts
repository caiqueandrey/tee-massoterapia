import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private fire: AngularFireAuth
  ) {}

  facebookLogin(){
    const provider = new auth.FacebookAuthProvider();
    this.fire.auth.signInWithPopup(provider);
  }

}
