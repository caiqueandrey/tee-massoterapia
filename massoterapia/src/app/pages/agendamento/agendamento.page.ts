import { Component, OnInit } from '@angular/core';
import { Agendamento } from '../../interfaces/agendamento';
import { NavController, AlertController, ToastController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AgendamentoService } from '../../services/agendamento.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-agendamento',
  templateUrl: './agendamento.page.html',
  styleUrls: ['./agendamento.page.scss'],
})
export class AgendamentoPage implements OnInit {
  dataServicos: any;
  dataProfissionais: any;
  private agendamentoId: string = null;
  private agendamentoSubscription: Subscription;
  agendamento: Agendamento = {};
  private loading: any;

  constructor(
    private http: HttpClient,
    private alert: AlertController,
    private db: AngularFirestore,
    private activeRoute: ActivatedRoute,
    private nav: NavController,
    private toast: ToastController,
    private agendamentoService: AgendamentoService
  ) {
    this.agendamentoId = this.activeRoute.snapshot.params['id'];
    if(this.agendamentoId) this.loadAgendamento();
   }

  ngOnInit() {
    fetch('./assets/data/servicos.json').then(res => res.json()).then(json => {
      this.dataServicos = json;
    });
    fetch('./assets/data/profissionais.json').then(res => res.json()).then(json => {
      this.dataProfissionais = json;
    });
  }

  ngOnDestroy() {
    if (this.agendamentoSubscription) this.agendamentoSubscription.unsubscribe();
  }

  loadAgendamento() {
    this.agendamentoSubscription = this.agendamentoService.getAgendamento(this.agendamentoId).subscribe(data => {
      this.agendamento = data;
    });
  }

  async agendar(){
    if(this.agendamentoId){
      try {
        await this.agendamentoService.updateAgendamento(this.agendamentoId, this.agendamento);
        await this.loading.dismiss();

        this.nav.navigateBack('/home');
      } catch (error) {
        this.presentToast('Erro ao tentar salvar');
        this.loading.dismiss();
      }
    } else {
      this.agendamento.createdAt = new Date().getTime();
      
      try {
        await this.agendamentoService.addAgendamento(this.agendamento);
        await this.loading.dismiss();

        this.nav.navigateBack('/agendamento');
      } catch (error) {
        this.presentToast('Erro ao tentar adicionar');
        this.loading.dismiss();
      }
    }
  }

  async presentToast(message: string) {
    const toast = await this.toast.create({ message, duration: 2000 });
    toast.present();
  }
}
