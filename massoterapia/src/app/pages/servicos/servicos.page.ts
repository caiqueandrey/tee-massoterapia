import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servicos',
  templateUrl: './servicos.page.html',
  styleUrls: ['./servicos.page.scss'],
})
export class ServicosPage implements OnInit {
  dataServicos: any;
  dataProfissionais: any;

  constructor() { }

  ngOnInit() {
    fetch('./assets/data/servicos.json').then(res => res.json()).then(json => {
      this.dataServicos = json;
    });
    fetch('./assets/data/profissionais.json').then(res => res.json()).then(json => {
      this.dataProfissionais = json;
    });
  }

}
