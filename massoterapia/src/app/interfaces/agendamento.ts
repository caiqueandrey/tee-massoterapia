export interface Agendamento {
    id?: string;
    servico?: string;
    profissional?: string;
    data?: number;
    valor?: string;
    pagamento?: string;
    endereco?: string;
    createdAt?: number;
    userId?: string;
}
