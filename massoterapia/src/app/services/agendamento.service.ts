import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Agendamento } from '../interfaces/agendamento';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AgendamentoService {
  private agendamentoCollection : AngularFirestoreCollection<Agendamento>;

  constructor(private fire: AngularFirestore) {
    this.agendamentoCollection = this.fire.collection<Agendamento>('agendamento');
   }

   getAgendamentos(){
     return this.agendamentoCollection.snapshotChanges().pipe(
       map(actions => {
         return actions.map(a => {
           const data = a.payload.doc.data();
           const id = a.payload.doc.id;

           return { id, ...data };
         })
       })
     )
   }

   addAgendamento(agendamento: Agendamento){
     return this.agendamentoCollection.add(agendamento);
   }

   getAgendamento(id: string){
    return this.agendamentoCollection.doc<Agendamento>(id).valueChanges();
   }

   updateAgendamento(id:string, agendamento: Agendamento){
     return this.agendamentoCollection.doc<Agendamento>(id).update(agendamento);
   }

  deletandoAgendamento(id: string) {
    return this.agendamentoCollection.doc(id).delete();
  }
}
