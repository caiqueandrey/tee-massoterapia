import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'servicos', loadChildren: () => import('./pages/servicos/servicos.module').then( m => m.ServicosPageModule)},
  { path: 'agendamento', loadChildren: () => import('./pages/agendamento/agendamento.module').then( m => m.AgendamentoPageModule)},
  { path: 'perfil', loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule)},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
