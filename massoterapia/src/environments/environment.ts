// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
        apiKey: "AIzaSyC2bZM2m8CGXr7ZvpB6c_23CjPxWB7A2cY",
        authDomain: "tee-massoterapia.firebaseapp.com",
        databaseURL: "https://tee-massoterapia.firebaseio.com",
        projectId: "tee-massoterapia",
        storageBucket: "tee-massoterapia.appspot.com",
        messagingSenderId: "728012749810",
        appId: "1:728012749810:web:f253da0fb98f2299e0c96b"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
